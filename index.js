const express = require("express");
// Mongoose is a package that allows the creation of Schemas to model data structures
// Also has access to a number of methods for manipulating databases
const mongoose = require("mongoose");

const app = express();
const port = 3001;

// [SECTION] MongoDB Connection
// Connect to the database by passing in your connection string, remember to replace placeholder password and database names

mongoose.connect("mongodb+srv://admin:admin@batch230.cqzdm3c.mongodb.net/S35?retryWrites=true&w=majority",
{
    useNewUrlParser: true,
    useUnifiedTopology: true
})

// Connection to database
// Allows to handle errors when the initial connection is established
// Works with the on and once Mongoose methods
let db = mongoose.connection;

// If a conn. error occures, putput in the console
// console.error.bind(console) allows us to print errors in the browser console and in the terminal

db.on("error", console.error.bind(console, "connection error"));

db.once("open", () => {console.log("We're connected to the cloud database")})

app.use(express.json());

const taskSchema = new mongoose.Schema({
    name: String,
    status: {
        type: String,
        default: "pending"
    }
})

const Task = mongoose.model("Task", taskSchema);

// =======================================================
// Check if there are duplicate tasks
app.post("/tasks", (req, res) => {

    Task.findOne({name: req.body.name}, (err, result) => {

        if( result !== null && result.name === req.body.name){

            return res.send("Duplicate task found")

        } else {

            let newTask = new Task({
                name: req.body.name
            });

            newTask.save((saveErr, savedTask) => {
                if(saveErr) {
                    return console.error(saveErr);
                } else {
                    return res.status(201).send("New task created");
                }
            });

        }
        
    });

})

// ===========================================

app.get("/tasks", (req, res) => {

    Task.find({}, (err, result) => {

        if(err) {
            return console.log(err);
        } else {
            return res.status(200).json({
                data : result 
            })
        }

    });

})

// ========================================
// S35 - ACTIVITY CODE
// ========================================

// Business Logic
/*
1. Add a functionality to check if there are duplicate tasks
	- If the user already exists in the database, we return an error
	- If the user doesn't exist in the database, we add it in the database
2. The user data will be coming from the request's body
3. Create a new User object with a "username" and "password" fields/properties
*/

const userSchema = new mongoose.Schema({
    username: String,
    password: String
})

const User = mongoose.model("User", userSchema);

// Adding a User

app.post("/users", (req, res) => {
    User.findOne({username : req.body.username}, (err, result) => {
        if (result != null && result.username == req.body.username) return res.send("Duplicate user found!");

        // if statement serves as a "checkpoint", thus no need for else statement
        // readability purposes

        let newUser = new User({
            username: req.body.username,
            password: req.body.password
        })

        newUser.save((saveErr, savedUser) => {
            if (saveErr) return console.error(saveErr);

            return res.status(201).send("New User added");
        });

    })
});

// retrieving all users

app.get("/users", (req, res) => {

    User.find({}, (err, result) => {
        if (err) return console.error(err);

        return res.status(200).json(
            {
                data : result
            }
        )
    })

});

app.listen(port, () => console.log(`Server running at port ${port}`));